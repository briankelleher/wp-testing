<?php
/*
Plugin Name: UConn In The News
Description: Displays links to UConn appearances from credible news sources.
Author: Brian Kelleher
Version: 1.0
*/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

// Adds on widget hook / registers widget
add_action( 'widgets_init', function() {
	register_widget('UConn_In_The_News');
});

/**
* Widget logic
*/
class UConn_In_The_News extends WP_Widget {

	/**
	* Construct widget
	*/
	function __construct() {

		$args = array(
			'description' => 'Display UConn in the News!'
		);

		parent::__construct(
			'UConn_In_The_News', // Base ID
			'UConn In The News', // Name
			$args // Widget custom args
		);

	}


	/**
	* Widget front-end
	*
	* @param array $args Widget arguments.
	* @param array $instance Saved values from database.
	*/
	public function widget( $args, $instance ) {

        // enqueue script
        wp_enqueue_script(
            'uc_in_the_news',
            get_stylesheet_directory_uri() . '/js/uc_in_the_news.js',
            array( 'jquery' )
        );

        // enqueue styles
        wp_enqueue_style(
            'uc_in_the_news',
            get_stylesheet_directory_uri() . '/css/uc_in_the_news.css'
        );

        // google material icon font
        wp_enqueue_style(
            'google_icons',
            'https://fonts.googleapis.com/icon?family=Material+Icons'
        );

		// Post query.
		$post_query = array(
			'posts_per_page'	=> $instance['num_links'],
			'offset'			=> 0,
			'orderby'			=> 'date',
			'order'				=> 'ASC',
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'suppress_filters'	=> true,
			'tax_query'=> array(
				array(
					'taxonomy'	=> 'post_format',
					'field'		=> 'slug',
					'terms'		=> array( 'post-format-link' ),
				),
			),
		);

		$posts_array = get_posts( $post_query );

		// Before widget HTML
		echo $args['before_widget'];

		// Display title if exists
		if ( !empty($instance['title'])) {
			echo '<h2 class="uc_in_the_news_title">' . apply_filters( 'widget_title', $instance['title']) . '</h2>';
		}

		// Variables pertaining to custom fields
		$news_source_meta_slug = 'news_source';
		$story_url_meta_slug   = 'story_url';

		// Start list.
		?>
		<section class="uc_in_the_news_list">
            <div class="uc_in_the_news_slider">
		      <div class="uc_in_the_news_half news_top news_half_showing">
		<?php

		$count=0;
		// Loop through post array
		foreach( $posts_array as $post ) : setup_postdata( $post );
			$count++;
			$news_source = get_post_meta( $post->ID, $news_source_meta_slug, true);
			$story_url = get_post_meta( $post->ID, $story_url_meta_slug, true);

            // Number of links to put on each slide
            $num_slide_links = ((intval($instance['num_links']) / 2) + 1);

			// Separates top/bottom after 5 posts
			if ($count==$num_slide_links)  {
				echo '</div><div class="uc_in_the_news_half news_bot">';
			}
		?>
			<div class="uc_in_the_news_item">
				<h2>
					<a target="_blank" href="<?php echo $story_url; ?>"><?php echo $news_source; ?></a>
				</h2>
				<h4>
					<a href="<?php echo get_the_permalink( $post->ID ); ?>" ><?php echo get_the_title( $post->ID ); ?></a>
				</h4>
			</div>
		<?php
		endforeach;

		?>
                </div>
			</div>
			<a href="#/" class="uc_in_the_news_arrow arrow_up disabled">
                <i class="material-icons">keyboard_arrow_up</i>         
            </a>
			<a href="#/" class="uc_in_the_news_arrow arrow_down">
                <i class="material-icons">keyboard_arrow_down</i>
            </a>
		</section>
		<?php
		// End list/reset postdata
		wp_reset_postdata();

		// After widget HTML
		echo $args['after_widget'];

	}


	/**
	* Widget back-end
	*
	* @param array $instance Previously saved values from database.
	*/
	public function form( $instance ) {

		if( isset( $instance['title'] )) {
			$title = $instance['title'];
		} else {
			$title = __( 'UConn In The News', 'text_domain' );
		}

        if( isset( $instance['num_links'] )) {
            $num_links = $instance['num_links'];
        } else {
            $num_links = '10';
        }

		// Form HTML
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
			<?php _e( 'Title:' ); ?>
			</label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>">
		</p>

        <p>
            <label for="<?php echo $this->get_field_id( 'num_links' ); ?>">
            <?php _e( 'Number of Links:' ); ?>
            </label>
            <select class="widefat" id="<?php echo $this->get_field_id( 'num_links' ); ?>" name="<?php echo $this->get_field_name( 'num_links' ); ?>">
                <option value="4" <?php selected( $num_links, '4' ); ?>>4</option>
                <option value="6" <?php selected( $num_links, '6' ); ?>>6</option>
                <option value="8" <?php selected( $num_links, '8' ); ?>>8</option>
                <option value="10" <?php selected( $num_links, '10' ); ?>>10</option>
            </select>
        </p>
		<?php
	}


	/**
	* Widget update/sanitize
	*
	* @param array $new_instance Values just sent to be saved.
	* @param array $old_instance Previously saved values from database.
	*
	* @return array Updated safe values to be saved.
	*/
	public function update( $new_instance, $old_instance ) {

		$instance = array();
        // sanitation
		$instance['title'] = ( !empty( $new_instance['title'])) ? strip_tags( $new_instance['title']) : '';

        $instance['num_links'] = $new_instance['num_links'];

		return $instance;

	}

}