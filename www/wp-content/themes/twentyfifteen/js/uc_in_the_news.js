( function( $ ) {

    $( window ).load( function() {

        // in-the-news
        var slider_view        = $('section.uc_in_the_news_list'),
            slider_container   = $('div.uc_in_the_news_slider'),
            arrows             = $('a.uc_in_the_news_arrow'),
            up_arrow           = $('a.uc_in_the_news_arrow.arrow_up'),
            down_arrow         = $('a.uc_in_the_news_arrow.arrow_down'),
            top_half           = $('div.uc_in_the_news_half.news_top'),
            top_half_height    = top_half.height(),
            bottom_half        = $('div.uc_in_the_news_half.news_bot'),
            bottom_half_height = bottom_half.height(),
            adjust_number      = (top_half_height > bottom_half_height) ? top_half_height : bottom_half_height,
            top_adjust         = ((adjust_number + 5) * -1),
            resizeEvt;

        // Set heights on load
        slider_container.css( 'height', adjust_number + 'px' );
        top_half.css( 'height', adjust_number + 'px' );
        bottom_half.css( 'height', adjust_number + 'px' );

        // Bind resize events, fires only when done resizing
        $( window ).on("resize", function() {
            clearTimeout(resizeEvt);
            resizeEvt = setTimeout(newsReCalcHeights, 250);
        });


        function newsReCalcHeights() {

            // Reset heights
            slider_container.css( 'height', 'auto' );
            top_half.css( 'height', 'auto' );
            bottom_half.css( 'height', 'auto' );

            // New height calculations
            top_half_height    = top_half.height();
            bottom_half_height = bottom_half.height();
            adjust_number      = (top_half_height > bottom_half_height) ? top_half_height : bottom_half_height;

            // Set new heights
            slider_container.css( 'height', adjust_number + 'px' );
            top_half.css( 'height', adjust_number + 'px' );
            bottom_half.css( 'height', adjust_number + 'px' );

            // Set new top adjust for sliding
            top_adjust = ((adjust_number + 5) * -1);

            toggleNews(up_arrow);

        }

        function toggleNews(arrow) {

            // arrow toggle and content sliding
            if ( !arrow.hasClass('disabled') ) {
                if ( arrow.hasClass('arrow_up') ) {

                    top_half.css('top', '0px');
                    bottom_half.css('top', '0px');

                    arrow.toggleClass('disabled');
                    down_arrow.toggleClass('disabled');

                } else if ( arrow.hasClass('arrow_down') ) {

                    top_half.css('top', top_adjust + 'px');
                    bottom_half.css('top', top_adjust + 'px');

                    arrow.toggleClass('disabled');
                    up_arrow.toggleClass('disabled');

                }
            }

        }

        arrows.click(function(e) {

            toggleNews($(this));

        });
        //end in-the-news

    });

} )(jQuery);