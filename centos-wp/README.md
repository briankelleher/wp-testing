# Keep in mind

Make sure to alter the Vagrantfile with credentials that match your wp-config, including the path to your wordpress installation.

# Install

1. Add this repo as remote subtree `$ git remote add -f centos-wp git@bitbucket.org:ucomm/centos-wp.git`
1. Add subtree to project `$ git subtree add --prefix centos-wp centos-wp master --squash`
1. Push up to your repo `$ git push`

# Running the Box

```
$ cd centos-wp
$ vagrant up
```

# Updating

Has this repo been upgraded since your install?

1. `$ git fetch centos-wp master`
1. `$ git subtree pull --prefix centos-wp centos-wp master --squash`

Now you can commit fixes to this repo from your local working directory.  To commit your updates to the upstream, switch back to the project and subtree push.

1. `$ git subtree push --prefix centos-wp git@bitbucket.org:ucomm/centos-wp.git master`
