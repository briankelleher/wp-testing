# This

... is testing for wp plugins/widgets/modules.

# Config

```
$ git clone <repo-location>
$ cd <folder-created>
```

If you have a wordpress import, put it in the /www directory.

```
$ vagrant up
```

Pick a theme and start developing! No build process to allow separation of plugins/widgets and their dependencies :\(.